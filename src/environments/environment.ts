// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDbkODk0G3MwagyWnbJhZpio2oeSta3CJ4',
    authDomain: 'pokedex-02.firebaseapp.com',
    projectId: 'pokedex-02',
    storageBucket: 'pokedex-02.appspot.com',
    messagingSenderId: '93281577484',
    appId: '1:93281577484:web:78dc593bd21c6c3f0adc29',
    measurementId: 'G-D6NB65LZQF'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
