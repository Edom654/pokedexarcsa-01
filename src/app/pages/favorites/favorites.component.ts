import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { firestore } from 'firebase';


@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {

  listaFav: any = [];

  constructor() { }

  ngOnInit(): void {
    this.getFavorites();
  }

  getFavorites() {
    firebase.firestore().collection('favoritos').onSnapshot(data => {
      this.listaFav = [];
      data.forEach( favoritos => {
        this.listaFav = this.listaFav.concat(favoritos.data());
        console.log('Favorito', this.listaFav);
      });
    });
  }

}
