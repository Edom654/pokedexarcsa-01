import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { PokemonService } from '../../services/pokemon.service';
import * as firebase from 'firebase';
import { error } from '@angular/compiler/src/util';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  name: string;
  urlImage: string;
  urlImage2: string;
  descripcion: any = [];
  tipo: any [];
  habilidades: any = [];
  habilidades2: any = [];

  favoritos = {
    nombre: '',
    tipo: '',
    url: ''
  };

  constructor(private service: PokemonService, private router: Router) { }

  ngOnInit(): void {

  }

  getPokedex() {
    this.service.getPokemon(this.name).subscribe((data: any) => {
      this.urlImage = data.sprites.back_shiny;
      this.urlImage2 = data.sprites.front_default;
      this.tipo = data.types[0].type.name;
      this.habilidades = data.abilities[0].ability.name;
      this.habilidades2 = data.abilities[1].ability.name;
      this.descripcion = data;
      console.log('Info', this.descripcion);
    });
  }

  savePokemon() {
    if ( this.favoritos.nombre !== '' && this.favoritos.tipo !== '' && this.favoritos.url !== '' ) {
      firebase.firestore().collection('favoritos').add(this.favoritos).then( () => {
        console.log('Agregado');
        this.router.navigate(['/favorites']);
        $('#exampleModal').modal('hide');
      } ).catch(error);
    }
  }


}
